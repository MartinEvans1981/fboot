;/**************************************************************************//**
; * @file     startup_ARMCM4.s
; * @brief    CMSIS Core Device Startup File for
; *           ARMCM4 Device
; * @version  V5.4.0
; * @date     12. December 2018
; ******************************************************************************/
;/*
; * Copyright (c) 2009-2018 Arm Limited. All rights reserved.
; *
; * SPDX-License-Identifier: Apache-2.0
; *
; * Licensed under the Apache License, Version 2.0 (the License); you may
; * not use this file except in compliance with the License.
; * You may obtain a copy of the License at
; *
; * www.apache.org/licenses/LICENSE-2.0
; *
; * Unless required by applicable law or agreed to in writing, software
; * distributed under the License is distributed on an AS IS BASIS, WITHOUT
; * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; * See the License for the specific language governing permissions and
; * limitations under the License.
; */

;//-------- <<< Use Configuration Wizard in Context Menu >>> ------------------


;<h> Stack Configuration
;  <o> Stack Size (in Bytes) <0x0-0xFFFFFFFF:8>
;</h>

Stack_Size      EQU      0x00000700

                AREA     STACK, NOINIT, READWRITE, ALIGN=3
__stack_limit
Stack_Mem       SPACE    Stack_Size
__initial_sp


;<h> Heap Configuration
;  <o> Heap Size (in Bytes) <0x0-0xFFFFFFFF:8>
;</h>

Heap_Size       EQU      0x00000C00

                IF       Heap_Size != 0                      ; Heap is provided
                AREA     HEAP, NOINIT, READWRITE, ALIGN=3
__heap_base
Heap_Mem        SPACE    Heap_Size
__heap_limit
                ENDIF


                PRESERVE8
                THUMB


; Vector Table Mapped to Address 0 at Reset

                AREA     RESET, DATA, READONLY
                EXPORT   __Vectors
                EXPORT   __Vectors_End
                EXPORT   __Vectors_Size

__Vectors       DCD      __initial_sp                        ;     Top of Stack
                DCD      Reset_Handler                       ;     Reset Handler
                DCD      NMI_Handler                         ; -14 NMI Handler
                DCD      HardFault_Handler                   ; -13 Hard Fault Handler
                DCD      MemManage_Handler                   ; -12 MPU Fault Handler
                DCD      BusFault_Handler                    ; -11 Bus Fault Handler
                DCD      UsageFault_Handler                  ; -10 Usage Fault Handler
                DCD      0                                   ;     Reserved
                DCD      0                                   ;     Reserved
                DCD      0                                   ;     Reserved
                DCD      0                                   ;     Reserved
                DCD      SVC_Handler                         ;  -5 SVCall Handler
                DCD      DebugMon_Handler                    ;  -4 Debug Monitor Handler
                DCD      0                                   ;     Reserved
                DCD      PendSV_Handler                      ;  -2 PendSV Handler
                DCD      SysTick_Handler                     ;  -1 SysTick Handler

             ; Interrupts
                DCD      DMA_FLAG0_IRQHandler                  		;    0 Interrupt  0
                DCD      DMA_FLAG1_IRQHandler                  		;    1 Interrupt  1
                DCD      DMA_FLAG2_IRQHandler                  		;    2 Interrupt  2
                DCD      DMA_FLAG3_IRQHandler                  		;    3 Interrupt  3
                DCD      DMA_FLAG4_IRQHandler                  		;    4 Interrupt  4
                DCD      DMA_COMBI_IRQHandler                  		;    5 Interrupt  5
				DCD      GPIO_UART_OUT4_IRQHandler                  ;    6 Interrupt  6
                DCD      GPIO_UART_OUT3_IRQHandler                  ;    7 Interrupt  7
                DCD      GPIO_UART_OUT2_IRQHandler                  ;    8 Interrupt  8
                DCD      GPIO_UART_OUT1_IRQHandler                  ;    9 Interrupt  9
				DCD      GPIO_UART_IN4_IRQHandler                  	;   10 Interrupt 10
                DCD      GPIO_UART_IN3_IRQHandler                  	;   11 Interrupt 11
                DCD      GPIO_UART_IN2_IRQHandler                  	;   12 Interrupt 12
                DCD      GPIO_UART_IN1_IRQHandler                  	;   13 Interrupt 13
				DCD      GPIO_SPI_SCK_IRQHandler                  	;   14 Interrupt 14
                DCD      GPIO_SPI_DOUT_IRQHandler                  	;   15 Interrupt 15
                DCD      GPIO_SPI_DIN_IRQHandler                  	;   16 Interrupt 16
                DCD      GPIO_SPI_CS_0_IRQHandler                  	;   17 Interrupt 17
				DCD      GPIO_H_FN_IRQHandler                  		;   18 Interrupt 18
                DCD      GPIO_DE_IRQHandler                  		;   19 Interrupt 19
				DCD      UART3_IRQHandler                  			;   20 Interrupt 20
                DCD      UART2_IRQHandler                  			;   21 Interrupt 21
                DCD      UART1_IRQHandler                  			;   22 Interrupt 22
                DCD      UART0_IRQHandler                  			;   23 Interrupt 23
                DCD      TIMER2_IRQHandler             				;   24 Interrupt 24
                DCD      TIMER1_IRQHandler                 			;   25 Interrupt 25
                DCD      TIMER0_IRQHandler                  		;   26 Interrupt 26					
				DCD      SPI_S_TXO_IRQHandler           	     	;   27 Interrupt 27
				DCD      SPI_S_TXE_IRQHandler          				;   28 Interrupt 28
                DCD      SPI_S_RXU_IRQHandler          				;   29 Interrupt 29
				DCD      SPI_S_RXO_IRQHandler          				;   30 Interrupt 30
                DCD      SPI_S_RXF_IRQHandler          				;   31 Interrupt 31
                DCD      SPI_M_TXO_IRQHandler          				;   32 Interrupt 32
                DCD      SPI_M_TXE_IRQHandler          				;   33 Interrupt 33
                DCD      SPI_M_RXU_IRQHandler          				;   34 Interrupt 34
                DCD      SPI_M_RXO_IRQHandler          				;   35 Interrupt 35
                DCD      SPI_M_RXF_IRQHandler          				;   36 Interrupt 36
			    DCD      SPI_M_MST_IRQHandler          				;   37 Interrupt 37
				DCD      IC_SCL_STUCK_IRQHandler                  	;   38 Interrupt 38
                DCD      IC_TX_OVER_IRQHandler           			;   39 Interrupt 39
				DCD      IC_TX_EMPTY_IRQHandler           			;   40 Interrupt 40
                DCD      IC_TX_ARB_IRQHandler            			;   41 Interrupt 41
                DCD      IC_STOP_DET_IRQHandler           			;   42 Interrupt 42
                DCD      IC_START_DET_IRQHandler           			;   43 Interrupt 43
                DCD      IC_RX_UNDER_IRQHandler           			;   44 Interrupt 44
                DCD      IC_RX_OVER_IRQHandler           			;   45 Interrupt 45
                DCD      IC_RX_FULL_IRQHandler           			;   46 Interrupt 46
				DCD      IC_RX_DONE_IRQHandler           			;   47 Interrupt 47
                DCD      IC_RD_REQ_IRQHandler            			;   48 Interrupt 48
                DCD      IC_GEN_CALL_IRQHandler           			;   49 Interrupt 49
				DCD      IC_ACTIVITY_IRQHandler           			;   50 Interrupt 50
                DCD      EFUSE_PROG_IRQHandler           			;   51 Interrupt 51
                DCD      CLK_1K_COUNT_IRQHandler           			;   52 Interrupt 52
                DCD      RS485_RX_DET_IRQHandler           			;   53 Interrupt 53
		
					

           ;SPACE    (214 * 4)                           ; Interrupts 10 .. 224 are left out
		   SPACE    (171 * 4)                           ; Interrupts 10 .. 224 are left out
__Vectors_End
__Vectors_Size  EQU      __Vectors_End - __Vectors


                AREA     |.text|, CODE, READONLY

; Reset Handler

Reset_Handler   PROC
                EXPORT   Reset_Handler             [WEAK]
                IMPORT   SystemInit
                IMPORT   __main

                LDR      R0, =SystemInit
                BLX      R0
                LDR      R0, =__main
                BX       R0
                ENDP


; Macro to define default exception/interrupt handlers.
; Default handler are weak symbols with an endless loop.
; They can be overwritten by real handlers.
                MACRO
                Set_Default_Handler  $Handler_Name
$Handler_Name   PROC
                EXPORT   $Handler_Name             [WEAK]
                B        .
                ENDP
                MEND


; Default exception/interrupt handler

                Set_Default_Handler  NMI_Handler
                Set_Default_Handler  HardFault_Handler
                Set_Default_Handler  MemManage_Handler
                Set_Default_Handler  BusFault_Handler
                Set_Default_Handler  UsageFault_Handler
                Set_Default_Handler  SVC_Handler
                Set_Default_Handler  DebugMon_Handler
                Set_Default_Handler  PendSV_Handler
                Set_Default_Handler  SysTick_Handler

                 Set_Default_Handler  DMA_FLAG0_IRQHandler       
                Set_Default_Handler  DMA_FLAG1_IRQHandler       
                Set_Default_Handler  DMA_FLAG2_IRQHandler       
                Set_Default_Handler  DMA_FLAG3_IRQHandler       
                Set_Default_Handler  DMA_FLAG4_IRQHandler       
                Set_Default_Handler  DMA_COMBI_IRQHandler       
                Set_Default_Handler  GPIO_UART_OUT4_IRQHandler  
                Set_Default_Handler  GPIO_UART_OUT3_IRQHandler  
                Set_Default_Handler  GPIO_UART_OUT2_IRQHandler  
                Set_Default_Handler  GPIO_UART_OUT1_IRQHandler  
				Set_Default_Handler  GPIO_UART_IN4_IRQHandler   
                Set_Default_Handler  GPIO_UART_IN3_IRQHandler   
                Set_Default_Handler  GPIO_UART_IN2_IRQHandler   
                Set_Default_Handler  GPIO_UART_IN1_IRQHandler   
                Set_Default_Handler  GPIO_SPI_SCK_IRQHandler    
                Set_Default_Handler  GPIO_SPI_DOUT_IRQHandler   
                Set_Default_Handler  GPIO_SPI_DIN_IRQHandler    
                Set_Default_Handler  GPIO_SPI_CS_0_IRQHandler   
                Set_Default_Handler  GPIO_H_FN_IRQHandler       
                Set_Default_Handler  GPIO_DE_IRQHandler         
				Set_Default_Handler  UART3_IRQHandler           
                Set_Default_Handler  UART2_IRQHandler           
                Set_Default_Handler  UART1_IRQHandler           
                Set_Default_Handler  UART0_IRQHandler           
                Set_Default_Handler  TIMER2_IRQHandler          
                Set_Default_Handler  TIMER1_IRQHandler          
                Set_Default_Handler  TIMER0_IRQHandler          
                Set_Default_Handler  SPI_S_TXO_IRQHandler       
                Set_Default_Handler  SPI_S_TXE_IRQHandler       
                Set_Default_Handler  SPI_S_RXU_IRQHandler       
				Set_Default_Handler  SPI_S_RXO_IRQHandler       
                Set_Default_Handler  SPI_S_RXF_IRQHandler       
                Set_Default_Handler  SPI_M_TXO_IRQHandler       
                Set_Default_Handler  SPI_M_TXE_IRQHandler       
                Set_Default_Handler  SPI_M_RXU_IRQHandler       
                Set_Default_Handler  SPI_M_RXO_IRQHandler       
                Set_Default_Handler  SPI_M_RXF_IRQHandler       
                Set_Default_Handler  SPI_M_MST_IRQHandler       
                Set_Default_Handler  IC_SCL_STUCK_IRQHandler    
                Set_Default_Handler  IC_TX_OVER_IRQHandler      
				Set_Default_Handler  IC_TX_EMPTY_IRQHandler     
                Set_Default_Handler  IC_TX_ARB_IRQHandler       
                Set_Default_Handler  IC_STOP_DET_IRQHandler     
                Set_Default_Handler  IC_START_DET_IRQHandler    
                Set_Default_Handler  IC_RX_UNDER_IRQHandler     
                Set_Default_Handler  IC_RX_OVER_IRQHandler      
                Set_Default_Handler  IC_RX_FULL_IRQHandler      
                Set_Default_Handler  IC_RX_DONE_IRQHandler      
                Set_Default_Handler  IC_RD_REQ_IRQHandler       
                Set_Default_Handler  IC_GEN_CALL_IRQHandler     
				Set_Default_Handler  IC_ACTIVITY_IRQHandler     
				Set_Default_Handler  EFUSE_PROG_IRQHandler      
				Set_Default_Handler  CLK_1K_COUNT_IRQHandler    
				Set_Default_Handler  RS485_RX_DET_IRQHandler    

                ALIGN


; User setup Stack & Heap

                IF       :LNOT::DEF:__MICROLIB
                IMPORT   __use_two_region_memory
                ENDIF

                EXPORT   __stack_limit
                EXPORT   __initial_sp
                IF       Heap_Size != 0                      ; Heap is provided
                EXPORT   __heap_base
                EXPORT   __heap_limit
                ENDIF

                END
