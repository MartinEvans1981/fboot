#include <stdint.h>
#include <string.h>

#include "Flash.h"

#include "bootloader_api.h"

typedef struct 
{
    uint32_t address;
    uint32_t size;
    uint32_t xor_value;
}AppHeader;

/***********************************************************/

void die()
{
    GPIO test_gpio(SPI_CS_0_PIN);
    test_gpio.set_alternate_fucntion(GPIO_FUNC);
    
    while(1)
    {
        test_gpio.toggle();
    }
}

/***********************************************************/

void configure_swd()
{
    GPIO swdio(DE_PIN);
    GPIO swclk(H_FN_PIN);
    
    // According to "Hardware team"
    // a PU is necessary here
    swdio.set_mode(PULL_UP);
    
    swdio.set_output();
    swclk.set_output();
    
    swdio.set_alternate_fucntion(SWD_IO_AF);
    swclk.set_alternate_fucntion(SWD_CLK_AF);
}

/***********************************************************/


// The address of the other application's
// header in the external Flash
#define APP_HEADER_ADDRESS 0x00 

// Current temporary jump address - must 
// be as minimal as possible!
#define JUMP_ADDRESS 0x10003000 

// Parallel address to locate the other application
// in the RAM memory(this is basically the same
// address, the difference is in the memory
// access method
#define COPY_ADDRESS 0x20003000 

// The maximum available sapre 
// area to populate the other app
#define MAX_APP_SIZE 0x30000 

// Amount of byte to read from the 
// external flash each cycle
#define FLASH_READ_CHUNKS 0xFF 

// Location of the other App
// header within the external 
// Flash
#define APP_HEADER_ADDERSS 0x00 

uint8_t rx_buff[MAX_APP_SIZE] __attribute__((at(COPY_ADDRESS))) = {0};

int main()
{  
    char *ver_string = "Ver_1.6";
    
    uint32_t xor_value = 0;
    AppHeader app_header = {0};
    uint32_t read_size = FLASH_READ_CHUNKS;
    
    Flash flash(SPI_DOUT_PIN,
                SPI_DIN_PIN,
                SPI_SCK_PIN,
                SPI_CS_0_PIN,
                1000000);
    
    // Allow SWD connection
    configure_swd();
    
    if(flash.init() != SPIF_OK)
    {
        // Error - Flash initialization
        // didn't work
        die();
    }
    
    // Read the other application header so
    // the FBoot will be able to measure its 
    // start offset within the external Flash
    // and its size
    flash.read(&app_header,
               APP_HEADER_ADDERSS,
               sizeof(app_header));
    
    if((app_header.address == 0xFFFFFFFF) ||
        (app_header.size == 0xFFFFFFFF))
    {
        // Error - if the values are FFs it
        // means that the Flash is probably fully
        // erased
        die();
    }
    
    // Read the other application from the external Flash,
    // chunk by chunk, starting after the header location
    for(uint32_t i = 0; i < app_header.size; i += read_size)
    {
        
        // In the last transaction, only read the
        // amout of data we need
        if(app_header.size - i < read_size)
        {
            read_size = app_header.size - i;
        }
        
        flash.read(rx_buff + i, 
                   app_header.address + i,
                   read_size); 
    }
    
    // XOR verify the version
    for(uint32_t i = 0; i < app_header.size; i++)
    {
        xor_value = rx_buff[i] ^ xor_value;
    }
    
    if(xor_value != app_header.xor_value)
    {
        // Error - if the xor values 
        // didn't match - there is a problem...
        die();
    }
    
    start_application(JUMP_ADDRESS);
    
    return 0;
}




